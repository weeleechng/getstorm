<?php if(!defined('IN_GS')){ die('you cannot load this page directly.'); }
/****************************************************
*
* @File: 			template.php
* @Package:		GetSimple
* @Action:		SimpleStorm theme for GetSimple CMS
* @Usage:     Front Page - Scrolling
*
*****************************************************/
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php get_site_name(); ?></title>
	<?php get_header(); ?>
	<meta name="robots" content="index, follow" />
	<meta charset="utf-8">
	<link rel="shortcut icon" href="http://resources.estorm.com/icon/favicon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="http://resources.estorm.com/icon/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php get_theme_url(); ?>/css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php get_theme_url(); ?>/css/stylesheets/main.css" media="all" />
	<?php _storm_include_js (); ?>
	<script src="<?php get_theme_url(); ?>/js/storm_front_ajax_link.js"></script>
	<script src="<?php get_theme_url(); ?>/js/storm_slider_panel.js"></script>
	<script src="<?php get_theme_url (); ?>/js/storm_smooth_scroll.js"></script>
	<script src="<?php get_theme_url(); ?>/js/storm_nav_scroll_link.js"></script>
	<script src="<?php get_theme_url(); ?>/js/storm_windowstate.js"></script>
</head>
<body id="<?php get_page_slug(); ?>" class="front-scroller desktop-view" about="<?php get_site_url (); ?>">
	
	<?php get_page_content(); ?>

  <?php get_i18n_component ('top-navigation-bar'); ?>

  <?php get_i18n_component ('bottom-footer-bar'); ?>
	
	<?php _storm_execute_included_js (); ?>
	<script type="text/javascript">
$('div.rss-wrapper div.rss-drawer').addSliderOpenListener ();
$('div.navigation #navlinks a.link.internal').each (function (index, element) 
{ 
	$(element).scrollifyLink (); 
});
$(document).addScrollListener ();
$('body.front-scroller.desktop-view div.page a').each (function (index, element) 
{ 
  $(element).ajaxifyLink (); 
});

	</script>

</body>
</html>
