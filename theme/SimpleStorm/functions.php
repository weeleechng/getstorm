<?php

function get_content ($page)
{

  $item = array();

  $path = "data/pages";
  $thisfile = @file_get_contents ($path.'/'.$page.'.xml');
  $data = simplexml_load_string ($thisfile);
  
  $item['content'] = stripslashes (htmlspecialchars_decode($data->content, ENT_QUOTES));
  $item['template'] = strval ($data->template);
  $item['title'] = strval ($data->title);
  $item['pubDate'] = strval ($data->pubDate);
  $item['url'] = strval ($data->url);
  $item['private'] = strval ($data->private);
  $item['parent'] = strval ($data->parent);
  $item['menuOrder'] = strval ($data->menuOrder);
  
  return $item;
    
}

function get_include_content_slug ($current_slug = null)
{
  $pattern = '/{\%\sinclude-content\s\'(\w+)\'\s\%\}/U';
  preg_match ($pattern, get_content ($current_slug)['content'], $match);
  if (isset ($match[1]))
  {
    return $match[1];
  }
  else
  {
    return null;
  }
}

function get_website_settings ()
{
  $item = array();
  $path = "data/other";
  $thisfile = @file_get_contents ($path.'/website.xml');
  $data = simplexml_load_string ($thisfile);
  
  $item['sitename'] = stripslashes (htmlspecialchars_decode($data->SITENAME, ENT_QUOTES));
  $item['siteurl'] = strval ($data->SITEURL);
  $item['template'] = strval ($data->TEMPLATE);
  
  return $item;
}

function _storm_get_current_theme ()
{
  return get_website_settings()['template'];
}

function _storm_search_current_array ($input_array, $search_key)
{
  foreach ($input_array as $key => $item)
  {
    if ($key == $search_key)
    {
      return $item;
    }
    else
    {
      if (is_array ($item))
      {
        return _storm_search_current_array ($item, $search_key);
      }
      else
      {
        continue;
      }
    }
  }  
  return null;
}

function _storm_print_menu_item ($submenu_items, $active = null)
{
  if (is_array ($submenu_items))
  {
    print '<div class="view-submenu">';
    foreach ($submenu_items as $key => $child)
    {
      print '<div class="submenu-item">';
      print '<a ' . (($active == $key) ? 'class="active" ' : null) . 'href="' . $child['url'] . '">' . $child['title'] .  '</a>';
      print '</div>';
    }
    print '</div>';
  }

}

function _storm_get_topmenu_array ($current_slug = null)
{
  if (function_exists ('get_structured_menu_array'))
  {
    $current_slug = strval ($current_slug);
    $current_array = get_structured_menu_array ($current_slug);
    $top_key = array_keys ($current_array)[0];
    $top_array = get_structured_menu_array ($top_key);
    
    return $top_array[$top_key];
  }
  else
  {
    return null;
  }

}

function _storm_get_topmenu_title ($current_slug = null)
{
  if (function_exists ('get_structured_menu_array'))
  {
    $top_array = _storm_get_topmenu_array ($current_slug);
    return $top_array['title'];
  }
  else
  {
    return return_page_title ();
  }
}

function _storm_get_topmenu_slug ($current_slug = null)
{
  if (function_exists ('get_structured_menu_array'))
  {
    $top_array = _storm_get_topmenu_array ($current_slug);
    return $top_array['slug'];
  }
  else
  {
    return return_page_slug ();
  }
}

function _storm_find_topmenu_active ($array, $slug)
{
  foreach ($array['children'] as $key => $value)
  {
    if ($key == $slug)
    {
      return $key;
    }
    else
    {
      if (is_array ($value) && isset ($value['children']))
      {
        if (_storm_find_topmenu_active ($value, $slug) != null)
        {
          return $key;
        }
      }
      else
      {
        continue;
      }
    }
  }
  
  return null;
}

function _storm_get_topmenu_item ($current_slug = null, $active = null)
{
  $top_array = _storm_get_topmenu_array ($current_slug);

  if (is_array ($top_array) && isset ($top_array['children']))
  {
    $top_items = $top_array['children'];
    if ($active == null)
    {
      $active = _storm_find_topmenu_active ($top_array, $current_slug);
      print '<!-- ' . $active . ' -->';
    }
    _storm_print_menu_item ($top_items, $active);
  }
  else
  {
    return null;
  }
}

function _storm_get_submenu_item ($current_slug = null, $active = null)
{
  if (function_exists ('get_structured_menu_array'))
  {
    $current_slug = strval ($current_slug);
    $current_array = get_structured_menu_array ($current_slug);

    $submenu_items = _storm_search_current_array ($current_array, $current_slug);

    if (is_array ($submenu_items) && isset ($submenu_items['children']))
    {
      $submenu_items = $submenu_items['children'];
      _storm_print_menu_item ($submenu_items, $active);
    }

  }
}

function _storm_get_parent_array ($current_slug = null)
{
  if (function_exists ('get_structured_menu_array'))
  {
    $current_slug = strval ($current_slug);
    $current_array = get_structured_menu_array (return_parent ());
    $parent = _storm_search_current_array ($current_array, return_parent ());

    return $parent;
  }
}

function _storm_get_parent_url ($current_slug = null)
{
  $parent = _storm_get_parent_array ($current_slug);
  if (is_array ($parent) && isset ($parent['url']))
  {
    return $parent['url'];
  }
}

function _storm_get_parentmenu_item ($current_slug = null)
{
  $parent = _storm_get_parent_array ($current_slug);

  if (is_array ($parent) && isset ($parent['children']))
  {
    $submenu_items = $parent['children'];
    _storm_print_menu_item ($submenu_items, $hyperlink);
  }

}

function _storm_include_js ()
{
  ?>
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="<?php get_theme_url(); ?>/js/storm_page_height.js"></script>
  <script src="<?php get_theme_url(); ?>/js/storm_content_valign_middle.js"></script>
  <script src="<?php get_theme_url(); ?>/js/storm_drawer_panel.js"></script>
  <script src="<?php get_theme_url(); ?>/js/storm_nav_search_collapsible.js"></script>
  <?php
}

function _storm_execute_included_js ()
{
  ?>
   <script type="text/javascript">
  $('div.page').each (function (index, element) { $(element).fillWindow (); } );
  $('div.page div.wrapper').each (function (index, element) { $(element).middleAlign(); } );
  $('div.footer').addPanelOpenListener ();
  $('div.navigation #search').searchCollapse ();
  </script>
  <?php
}

?>
