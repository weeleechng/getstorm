(function ($) 
{
  function scrollifyThisLink (element, target)
  {
    $(element).addClass ('scroller');
    $(element).attr ({ 'rel':$(target).attr('id'), });
    $(element).on ('click.navscroll_link', function (clickevent) 
    {
      clickevent.preventDefault ();
      $.scrollToPageId ($(clickevent.target).attr ('rel'), true);
    });
  }

  $.fn.scrollifyLink = function (options) 
  {
    if (this.hasClass ('scroller'))
    {
      return this;
    }

    var element = this;

    var settings = $.extend (
    {
      target: $.getPageElementByUrl (this.attr ('href')),
    }, 
    options);

    if (settings.target !== false)
    {
      scrollifyThisLink (element, settings.target); 
      this.addClass ('scroller');
    }
    
    return this;
  };

}
(jQuery));
