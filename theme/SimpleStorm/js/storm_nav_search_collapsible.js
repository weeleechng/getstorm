(function ($) 
{
  var animSpeed = 250;

  function toggleCollapse (element, expandedWidth, collapsedWidth)
  {
    if ($(element).width() == expandedWidth)
    {
      $(element).stop ();
      $(element).animate
      (
        { width: collapsedWidth + 'px', },
        animSpeed
      );
    }
    else
    {
      $(element).stop ();
      $(element).animate
      (
        { width: expandedWidth + 'px', },
        animSpeed
      );
    }

  }

  $.fn.searchCollapse = function (options) 
  {
    var element = this;
    var defaultTrigger = this.find ('input[type=submit]');
    var defaultCheckField = this.find ('input[type=text]');

    var settings = $.extend (
    {
      trigger: $(defaultTrigger),
      checkField: $(defaultCheckField),
      expandedWidth: $(element).width(),
      collapsedWidth: $(defaultTrigger).width (),
      startCollapsed: true,
    }, 
    options);

    $(settings.checkField).val('');

    if (settings.startCollapsed == true /* && $(settings.checkField).val() == ''*/)
    {
      this.css ({ 'width':settings.collapsedWidth + 'px', });
    }

    $(settings.trigger).on ('click.search_collapsible', function (clickevent)
    {
      if (settings.checkField.val() == '')
      {
        clickevent.preventDefault ();
        toggleCollapse (element, settings.expandedWidth, settings.collapsedWidth);
        settings.checkField.focus ();
      }

    });
    
    return this;
  };

}
(jQuery));
