(function ($) 
{
  function resize_background_image (element)
  {
    var image_ratio = 1.5;
    var ratio = $(window).width () / $(window).height ();

    if (ratio >= image_ratio)
    {
      $(element).css ({ "background-size" : "100% auto", });
    }
    else
    {
      $(element).css ({ "background-size" : "auto " + $(window).height () + "px", });
    }

    if ($(document).height () > $(window).height ())
    {
      $(element).css (
      {
        "background-position" : "50% 0", 
      });
    }
    else
    {
      $(element).css (
      {
        "background-position" : "50%", 
      });
    }

  }

  $.fn.randomClass = function (options) 
  {
    var settings = $.extend (
    {
      classes: [ "americas", "sea", "emea", "china", ],
      bkg_element: null,
      logo_element: null,
    }, 
    options);

    if (settings.bkg_element == null)
    {
      settings.bkg_element = this;
    }

    var andYourNumberIs = Math.floor((Math.random() * settings.classes.length));
    $(settings.bkg_element).addClass (settings.classes[andYourNumberIs]);
    $(settings.logo_element).addClass (settings.classes[andYourNumberIs]);

    $(window).on ('resize.fit_background', function () 
    { 
      resize_background_image (settings.bkg_element); 
    });

    $(document).on ('ready.fit_background', function ()
    {
      resize_background_image (settings.bkg_element);
    });

    this.addClass ('home-bkg-processed');

    return this;
  };

}
(jQuery));
