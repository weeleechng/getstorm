(function ($) 
{
  function ajaxifyLoad (element)
  {
    var targetLocation = $(element).attr ('rel');
    var targetUrl = $(element).attr ('href');

    $(targetLocation).load 
    (
      targetUrl + ' div.page div.wrapper', 
      function (response, status, xhr) 
      {
        /* ajaxify new document's links */
        $(targetLocation + ' a').each (function (index, item) 
        { 
          $(item).ajaxifyLink (); 
        });

        /* vertical-align new document's content */
        $(targetLocation + ' div.wrapper').middleAlign ();

        /* update browser URL */
        $.updateUrl (targetUrl, true, element);
      } 
    );
  }

  function ajaxifyThisLink (element, rel_element)
  {
    var rel_identifier = ($(rel_element).attr ('id') !== undefined) ? 'div#' + $(rel_element).attr ('id') :  'div.' + $(rel_element).attr ('class').replace (' ', '.');

    $(element).addClass ('ajax-link');
    $(element).attr ({'rel':rel_identifier, });
    $(element).off ('click.ajax_link');
    $(element).on ('click.ajax_link', function (clickevent)
    {
      clickevent.preventDefault ();
      ajaxifyLoad (element);
    });
  }

  $.ajax_load = function (element)
  {
    ajaxifyLoad (element);
  }

  $.fn.ajaxifyLink = function (options) 
  {
    /* ignore ajax-link-ed or target="_blank" hyperlinks */
    if (
      this.hasClass ('ajax-link') || 
      this.attr ('target') != typeof undefined &&
      this.attr ('target') == '_blank'
      )
    {
      return this;
    }

    var element = this;

    var settings = $.extend (
    {
      rel: this.parents ('div.page'),
    }, 
    options);

    ajaxifyThisLink (element, settings.rel); 
    this.addClass ('ajax-link');

    return this;
  };

}
(jQuery));
