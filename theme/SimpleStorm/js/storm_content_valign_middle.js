(function ($) 
{
  function alignToMiddle (element, relativeTo, extraOffset)
  {
    $(element).css ({ 'padding-top' : '', });

    var elementHeight = $(element).height ();
    // console.log ($(relativeTo).height () + " " + elementHeight);
    if ($(relativeTo).height () > elementHeight)
    {
      var offset_top = ($(relativeTo).height () - elementHeight - extraOffset) / 2;
      $(element).css ({ 'padding-top' : offset_top + "px", });
    }
  }

  $.fn.middleAlign = function (options) 
  {
    if (this.hasClass ('no-middlealign'))
    {
      return this;
    }

    var settings = $.extend (
    {
      elements: null,
      relativeTo: this.parent (),
      extraOffset: 0,
    }, 
    options);

    if (settings.elements == null)
    {
      var element = this;
      
      $(window).off ('resize.content_valign');
      $(window).on ('resize.content_valign', function () 
      { 
        alignToMiddle (element, settings.relativeTo, settings.extraOffset); 
      });

      alignToMiddle (element, settings.relativeTo, settings.extraOffset); 

    }
    
    this.addClass ("middlealign-processed");
    return this;
  };

}
(jQuery));
