(function ($) 
{
  function html5statechange (clicked)
  {
    // Change URL if option is selected and for html5 compatible browsers.
    if (window.history.replaceState) 
    {
      // store current url.
      var clickedClass = "." + $(clicked).attr ("class").replace(/\s/g, ".");
      var activeSibling = $(clickedClass).closest (".view-content").find ("a.active");
      var backSibling = $(clickedClass).closest (".view-content").find (".subcontent-backbutton a");
      var pushSelector;

      var currentURL;
      if ($(activeSibling).length > 0)
      {
        currentURL = $(activeSibling).attr ("href");
        pushSelector = selector;
      }
      else if ($(backSibling).length > 0)
      {
        currentURL = $(backSibling).attr ("href");
        pushSelector = $(backSibling).attr ("rel");
      }
      else
      {
        currentURL = document.URL;
        pushSelector = selector;
      }

      var currentURL = document.URL;

      var currentHistoryState = window.history.state;
      if (!currentHistoryState)
      {
        currentHistoryState = new Object ();
      }
      
      currentHistoryState.loadTarget = pushSelector;
      currentHistoryState.clickObj = clickedClass;

      window.history.replaceState (currentHistoryState, null, currentURL);
      window.history.pushState (null, null, removeHashFromString (url));

    }
  }

  function scroll_update_page_url (newUrl, pushevent)
  {
    var currentHistoryState = window.history.state;
    if (!currentHistoryState)
    {
      currentHistoryState = new Object ();
    }

    if (window.location.href != newUrl)
    {
      currentHistoryState.scrollEvent = "1";
      window.history.replaceState (currentHistoryState, null, window.location.href);

      if (pushevent == true)
      {
        console.log ('push: ' + newUrl);
        window.history.pushState ({ scrollEvent: "1" }, null, newUrl);
        console.log ('scroll pushstate ' + newUrl);
      }
      else
      {
        console.log ('repl: ' + newUrl);
        window.history.replaceState ({ scrollEvent: "1" }, null, newUrl);
        console.log ('scroll replacestate ' + newUrl);
      }
    }
  }

  function click_update_page_url (newUrl, clickObject)
  {
    var currentURL = document.URL;
    var currentHistoryState = window.history.state;

    if (!currentHistoryState)
    {
      currentHistoryState = new Object ();
    }

    if (window.location.href != newUrl)
    {
      currentHistoryState.clickObj = $(clickObject).attr ('href');
      currentHistoryState.pushSelector = $(clickObject).attr ('rel');
      window.history.replaceState (currentHistoryState, null, currentURL);
      window.history.pushState (null, null, newUrl);
      console.log ('click ' + newUrl);
    }
  }

  window.onpopstate = function (popevent) 
  {
    // when click this will get the 'current url' (since onpopstat is called 
    // AFTER the url change) and telling him not to add it to the state list.
    
    var stateObj = popevent.state;
    if (stateObj)
    {
      console.log ('pop');
      console.log (stateObj);

      // if (stateObj.loadTarget != undefined && stateObj.clickObj != undefined) 
      if (stateObj.clickObj != undefined) 
      { 
        //window.history.state is to tell the system if it was a 'soft' link or a
        // 'hard link' ie if there was a page in between taht was not managed by 
        // this script.
        // var selector = stateObj.loadTarget; // get rel from state object
        // $.ajax_load (stateObj.clickObj);

        /*
        if (!ajaxLoading && selector != "")
        {
          // ajaxLoading = true;
          // ajaxBefore (selector);
          // ajaxLink ($(stateObj.clickObj), selector, document.URL, context, false);
        }
        */
      }

    }
  }
  
  $.updateUrl = function (inUrl, pushevent, clickObject)
  {
    if (clickObject == undefined)
    {
      scroll_update_page_url (inUrl, pushevent);
    }
    else
    {
      click_update_page_url (inUrl, clickObject);
    }
  }

}
(jQuery));
