(function ($) 
{
  var animSpeed = 1000;

  function toggleDrawerPanel ()
  {
    if ($.targetPanel.height () == $.targetPanel_closedHeight)
    {
      $.targetPanel.stop ();
      $.targetPanel.animate (
        { height: $(window).height () + 'px', },
        animSpeed,
        function ()
        {
          $(this).addClass ('expanded');
          $.triggerButton.element.html ($.triggerButton.labelClose);
          $.triggerButton.element.removeClass ($.triggerButton.classOpen);
          $.triggerButton.element.addClass ($.triggerButton.classClose);
        }
      );
    }
    else
    {
      $.targetPanel.stop ();
      $.targetPanel.animate (
        { height: $.targetPanel_closedHeight + 'px', },
        animSpeed,
        function ()
        {
          $(this).removeClass ('expanded');
          $.triggerButton.element.html ($.triggerButton.labelOpen);
          $.triggerButton.element.removeClass ($.triggerButton.classClose);
          $.triggerButton.element.addClass ($.triggerButton.classOpen);
        }
      );
    }
  }

  $.fn.addPanelOpenListener = function (options) 
  {
    var element = this;

    var settings = $.extend (
    {
      trigger: $('div.footer div.footer-header #contact-us-link a'),
      closedHeight: $('div.footer div.footer-header').height (),
      closeButtonLabel: 'Close',
      closeButtonClass: 'button close',
    }, 
    options);

    $(document).on ('ready.contact_panel', function ()
    {
      $.targetPanel = element;
      $.targetPanel_closedHeight = settings.closedHeight;
      $.triggerButton = new Object ();
      $.triggerButton.element = settings.trigger;
      $.triggerButton.labelOpen = $(settings.trigger).html ();
      $.triggerButton.labelClose = settings.closeButtonLabel;
      $.triggerButton.classOpen = $(settings.trigger).attr ('class');
      $.triggerButton.classClose = settings.closeButtonClass;

      $(settings.trigger).on ('click.drawer_panel', function (clickevent)
      { 
        clickevent.preventDefault ();
        toggleDrawerPanel ();
      });
    });

    this.addClass ('drawer-panel');

    return this;
  };


  $.togglePanel = function ()
  {
    toggleDrawerPanel ();
  }

}
(jQuery));
