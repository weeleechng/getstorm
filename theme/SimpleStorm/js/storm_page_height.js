(function ($) 
{
  function elementSetMinHeightToWindow (element)
  {
    $(element).css ({ 'min-height':$(window).innerHeight() + 'px', });
  }

  $.fn.fillWindow = function (options) 
  {
    var settings = $.extend (
    {
      elements: null,
    }, 
    options);

    if (settings.elements == null)
    {
      var element = this;
      
      $(window).on ('resize.page_height', function () 
      {
        elementSetMinHeightToWindow (element);        
      });

      elementSetMinHeightToWindow (this);
    }
    
    return this;
  };

}
(jQuery));
