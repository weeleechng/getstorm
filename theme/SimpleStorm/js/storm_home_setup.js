(function ($) 
{
  $.fn.homeSetup = function (options) 
  {
    var settings = $.extend (
    {
      element_btn: null,
      element_btn_label_open: '[+] more',
      element_btn_label_close: '[&ndash;] hide',
      element_top: null,
      padding_element_top: "35px",
      element_copy: null,
      element_copy_content: null,
      element_wrapper: null,
      speed: 750,
    }, 
    options);

    $(settings.element_btn).on ('click.home_setup', function (clickevent)
    {
      if ($(settings.element_copy).height() == 0)
      {
        $(settings.element_btn).html (settings.element_btn_label_close);
        $(settings.element_top).animate ({ "padding-top":"0", }, settings.speed);
        $(settings.element_copy).animate ({ height: ($(settings.element_copy_content).height()) + "px", }, settings.speed);
        $(settings.element_wrapper).animate ({ backgroundColor: "rgba(0,0,0,0.5)", }, settings.speed);
      }
      else if ($(settings.element_copy).height() >= $(settings.element_copy_content).height())
      {
        $(settings.element_btn).html (settings.element_btn_label_open);
        $(settings.element_top).animate ({ "padding-top": settings.padding_element_top, }, settings.speed);
        $(settings.element_copy).animate ({ height: "0px", }, settings.speed);
        $(settings.element_wrapper).animate ({ backgroundColor: "transparent", }, settings.speed);
      }

    });

    this.addClass ('home-setup-processed');

    return this;
  };

}
(jQuery));
