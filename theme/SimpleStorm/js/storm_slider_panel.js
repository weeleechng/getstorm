(function ($) 
{
  var animSpeed = 333;

  function getMaxHeight (elementset)
  {
    var maxHeight = 0;
    $(elementset).each (function (index, element)
    {
      if ($(element).height () > maxHeight)
      {
        maxHeight = $(element).height ();
      }
    });

    return maxHeight;
  }

  function resetMaxHeight (elementset)
  {
    $(elementset).each (function (index, element)
    {
      $(element).css ({ 'height':'', });
    });

    return maxHeight;
  }

  function removeHoverListener (element)
  {
    $(element).off ('mouseenter.slider_panel');
    $(element).off ('mouseleave.slider_panel');
  }

  function addHoverListener (element, item, xHeight)
  {
    $(element).on ('mouseenter.slider_panel',
    function (hoverevent) 
    {
      $(item).stop ();
      $(item).animate (
        { height: xHeight + 'px', },
        animSpeed,
        function () { $(element).addClass ('expanded'); }
      );
    });

    $(element).on ('mouseleave.slider_panel',
    function (hoverevent) 
    { 
      $(item).stop ();
      $(item).animate (
        { height: '0px', },
        animSpeed,
        function () { $(element).removeClass ('expanded'); }
      );
    });
  }

  function collapseItem (element)
  {
    $(element).css ({ 'height': '0px', });
  }

  function uniformHeight (elementset)
  {
    var maxHeight = 0;
    
    $(elementset).each (function (index, element)
    {
      $(element).css ({ 'min-height':'', });
    });

    $(elementset).each (function (index, element)
    {
      if ($(element).height () > maxHeight)
      {
        maxHeight = $(element).height ();
      }
    });

    $(elementset).each (function (index, element)
    {
      $(element).css ({ 'min-height':maxHeight + 'px', });
    });
  }

  function positionTitleItem (element, offsetBottom)
  {
    $(element).css ({ 'bottom':offsetBottom + 'px', });
  }

  $.fn.addSliderOpenListener = function (options) 
  {
    var element = this;

    var settings = $.extend (
    {
      titleItem: '.more-blogposts',
      actionItems: '.rss-item',
      uniformItems: '.rss-title',
      targetItems: '.rss-content',
      maxHeightItems: '.rss-item .rss-content',
    }, 
    options);

    $(window).on ('load.slider_panel', function ()
    {
      uniformHeight (settings.uniformItems);
      maxHeight = getMaxHeight (settings.maxHeightItems);
      $(settings.actionItems).each (function (index, item)
      {
        collapseItem ($(item).find (settings.targetItems));
        positionTitleItem ($(element).parent().find (settings.titleItem), $(element).outerHeight());
        addHoverListener ($(item), $(item).find (settings.targetItems), maxHeight);
      });
    });

    $(window).resize (function ()
    {
      uniformHeight (settings.uniformItems);
      resetMaxHeight (settings.maxHeightItems);
      maxHeight = getMaxHeight (settings.maxHeightItems);
      $(settings.actionItems).each (function (index, item)
      {
        removeHoverListener ($(item));
        collapseItem ($(item).find (settings.targetItems));
        positionTitleItem ($(element).parent().find (settings.titleItem), $(element).outerHeight());
        addHoverListener ($(item), $(item).find (settings.targetItems), maxHeight);
      });
    });

    this.addClass ('slider-panel');

    return this;
  };

}
(jQuery));
