(function ($) 
{
  var scrollSpeed = 1000;
  var checkDelay = 500;
  var ignoreUrl;

  function get_scrollspeed (target)
  {
    var difference = Math.abs ($(document).scrollTop () - ($(target).offset().top));
    return (difference > scrollSpeed) ? scrollSpeed : difference;
  }

  function do_smoothscroll (target)
  {
    if ($(target).length == undefined || $(target) == false)
    {
      return false;
    }

    var dest = $(target).offset().top;

    if ($(document).scrollTop() != dest)
    {
      $('html,body').animate (
      {
        scrollTop: dest,
      }, 
      get_scrollspeed ($(target)));
    }
  }

  function get_target_by_url (url)
  {
    var target = false;

    $.each ($("body.front-scroller.desktop-view div.page"), function (index, value) 
    {
      if ($(value).attr ("about") == url)
      {
        target = value;
        return false;
      }
    });

    return target;
  }

  function get_target_by_id (inId)
  {
    var target = false;

    $.each ($("body.front-scroller.desktop-view div.page"), function (index, value) 
    {
      if ($(value).attr ("id") == inId)
      {
        target = value;
        return false;
      }
    });

    if (target == false)
    {
      target = $("body.front-scroller.desktop-view div.page").first ();
    }

    return target;
  }

  function update_active_nav_link (element, targets, target_attr)
  {
    var search_key = $(element).attr ('id');

    $(targets).each (function (index, value)
    { 
      $(value).removeClass ('active'); 
    });

    $.each ($(targets), function (index, value)
    {
      if ($(value).attr (target_attr) == search_key)
      {
        $(value).addClass ('active');
      }
    });
  }

  function check_scroll_pos (element, markers, targets, target_attr)
  {
    var pos = $(element).scrollTop();
    $.each ($(markers), function (index, value) 
    {
      diff = Math.abs (pos - ($(value).offset().top));
      if (diff < ($(window).height() / 5))
      {
        update_active_nav_link (value, targets, target_attr);
        if (window.location.href != $(value).attr ('about'))
        {
          if ($(value).attr ('about') == ignoreUrl)
          {
            $.updateUrl ($('body').attr('about'), false);
          }
          else
          {
            $.updateUrl ($(value).attr ('about'), false);
          }
        }
        return false; /* exit $.each loop */
      }
    });
  }

  $.fn.addScrollListener = function (options) 
  {
    var element = this;
    var scrollCheckTimer;

    var settings = $.extend (
    {
      markers: $('body.front-scroller.desktop-view div.page'),
      targets: $('.navigation #navlinks a.link'),
      target_attr: 'rel',
    }, 
    options);

    $(window).load (function ()
    {
      /* ignore /home URL */
      ignoreUrl = $(settings.markers).first().attr ('about');

      $(element).on ('scroll.add_scroll_listener', function ()
      {
        if (scrollCheckTimer)
        {
          clearTimeout (scrollCheckTimer);
        }

        scrollCheckTimer = setTimeout (function () { check_scroll_pos (element, settings.markers, settings.targets, settings.target_attr); }, checkDelay);

      });

      scrollCheckTimer = setTimeout (function () { check_scroll_pos (element, settings.markers, settings.targets, settings.target_attr); }, checkDelay);

    });

    return this;
  };


  $.getPageElementByUrl = function (inUrl)
  {
    return get_target_by_url (inUrl);
  }

  $.scrollToPageId = function (inId, pushstate)
  {
    var scrolltarget = get_target_by_id (inId);
    do_smoothscroll (scrolltarget);
    $.updateUrl ($(scrolltarget).attr ('about'), pushstate);
  }

}
(jQuery));
