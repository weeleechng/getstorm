<?php if(!defined('IN_GS')){ die('you cannot load this page directly.'); }
/****************************************************
*
* @File: 			template.php
* @Package:		GetSimple
* @Action:		SimpleStorm theme for GetSimple CMS
* @Usage:     Top Menu Level and Title only
*
*****************************************************/

?>
<!DOCTYPE html>
<html>
<head>
	<title><?php get_page_clean_title(); ?> &mdash; <?php get_site_name(); ?></title>
	<?php get_header(); ?>
	<meta name="robots" content="index, follow" />
	<meta charset="utf-8">
	<link rel="shortcut icon" href="http://resources.estorm.com/icon/favicon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="http://resources.estorm.com/icon/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php get_theme_url(); ?>/css/normalize.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php get_theme_url(); ?>/css/stylesheets/main.css" media="all" />
	<?php _storm_include_js (); ?>
</head>
<body id="<?php get_page_slug(); ?>" class="desktop-view">

	<div class="page section-<?php echo _storm_get_topmenu_slug (return_page_slug ()); ?>" id="page-<?php get_page_slug(); ?>" about="<?php get_page_url (); ?>">
		<div class="wrapper standard">
			<div class="page-title">
			  <h2><?php print _storm_get_topmenu_title (return_page_slug ()); ?></h2>
			</div>
			<div class="submenu">
				<?php _storm_get_topmenu_item (return_page_slug ()); ?>
			</div>
			<div class="content">
				<div class="content-parentmenu">
					<?php _storm_get_parentmenu_item (return_page_slug ()); ?>
				</div>
				<?php get_page_content(); ?>
				<div class="content-submenu">
					<?php _storm_get_submenu_item (return_page_slug()); ?>
				</div>
			</div>
		</div>
	</div>

  <?php get_component('top-navigation-bar'); ?>

  <?php get_component('bottom-footer-bar'); ?>

	<?php _storm_execute_included_js (); ?>
	
</body>
</html>
