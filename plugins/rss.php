<?php

/* Get correct id for plugin. */
$thisfile = basename(__FILE__, ".php");

/* Register plugin. */
register_plugin(
	$thisfile, 				# ID of plugin, should be filename minus php
	'rss feeder', 					# Title of plugin
	'0.1', 						# Version of plugin
	'Mikkel Bundgaard',						# Author of plugin
	'http://www.notfound.dk', 						# Author URL
	'A small rss feed reader', 						# Plugin Description
	'rss link', 					# Page type of plugin
	'rss_show(RSS link)'  	# Function that displays content
);

function _rss_clear_cache ()
{
	$cachefile = GSDATAOTHERPATH . 'rss_cache.json';
  if (is_file ($cachefile)) 
  {
    unlink ($cachefile);
  }  
}

function _rss_cache_has_expired ($json_rss)
{
	$cache_datetime = date_create ($json_rss->created);
	$now_datetime = date_create ();
	$difference = $cache_datetime->diff ($now_datetime);

	if ($difference->h >= 1 || $difference->d > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function _rss_refresh_cache ($jsonfile, $url)
{
  $xmldom = new DOMDocument ();
  if (@$xmldom = simplexml_load_file ($url))
  {
  	$json_rss = array ();
  	$json_rss['created'] = date ('Y-m-d H:i:s');
  	$json_rss['feed'] = $xmldom->saveXML();
  	$json_output = json_encode ($json_rss);
  	file_put_contents ($jsonfile, $json_output);
  	return $json_output;
  }
}

function _rss_read_cache ($jsonfile)
{
	$file_contents = file_get_contents ($jsonfile);
	if ($file_contents !== false)
	{
		$json_rss  = json_decode ($file_contents);
	}
	else
	{
		$json_rss = false;
	}

	return $json_rss;
}

function _rss_read ($url)
{
	$jsonfile = GSDATAOTHERPATH . 'rss_cache.json';

	if (file_exists ($jsonfile))
	{
		$json_rss  = json_decode (file_get_contents ($jsonfile));
	}
	else
	{
		$json_rss = false;
	}
	
	if ($json_rss === false || _rss_cache_has_expired ($json_rss))
	{
		$json_rss = _rss_refresh_cache ($jsonfile, $url);
	}
	else
	{
		$json_rss = _rss_read_cache ($jsonfile);
	}
	return $json_rss;
}

function _rss_extract_dom ($url)
{
	$attempts = 0;
	$xml = null;
	do
	{
		if ($xml == null)
		{
			$xmlobj = _rss_read ($url);

  		if ($xmlobj == false)
  		{
  			break;
  		}

			$xml = simplexml_load_string ($xmlobj->feed);
  	}
		$attempts++;
	}
	while ($xml == null);

	if ($attempts != 0)
	{
		print "<!-- " . $attempts . " -->";
	}

	return $xml;
}

function show_rss ($url) 
{
	$xml = _rss_extract_dom ($url);

	foreach($xml->channel as $pChild) 
	{
		$itemCount = 0;
		foreach ($pChild->item as $pItem) 
		{
			$itemCount++;
			$namespaces = $pItem->getNameSpaces (true);

			$dc = $pItem->children ($namespaces['dc']);
			$author = $dc->creator;

		  foreach ($pItem->children() as $pChild)
			{
				switch ($pChild->getName()) 
			  { 
					case 'title': 
				    $title = $pChild;
				    break; 
				     
					case 'link': 
				    $link = $pChild;
				    break;

					case 'description': 
				    $description = $pChild;
				    break; 
					
					default: 
				    break; 
		    } 
			
				if (isset($link) && isset($title) && isset($description)) 
				{
					print '<div class="rss-item row-' . $itemCount . '">'; 
					printf ('<div class="rss-title"><h4><a href="%s" target="_blank">%s</a></h4><p class="rss-author">%s</p></div>', $link, $title, $author);
					print '<div class="rss-content">';
					printf ('<div id="rss:%s" class="rss-description">%s</div>', $link, $description);
					print '<div class="rss-readmorelink"><a href="'. $link . '" target="_blank">Read more</a></div>';
					print '</div>';
					print '<div class="pseudo-slider row-' . $itemCount . '"></div>';
					print '</div>';
					unset ($link);
					unset ($title);
					unset ($author);
					unset ($description);
				}
			}

			if ($itemCount >= 3)
			{
				break;
			}
		}
 	}  

}
 
?>
