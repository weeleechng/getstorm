<?php
/**
 * The plugin translates the flat menustructure of the GetSimple-standard-menu
 * into a structured one. The template-designer can fitler for submenus only and
 * can choose between an output as array or 'ul'-tagstructure.
 *
 * @link http://www.hofrichter.net
 * @since 10/2013
 * @version: 1.0
 * @author: Sven Hofrichter - v1.0 - 2013-10-23 - initial version
*/

// ID of the plugin
$thisfile=basename(__FILE__, ".php");

// plugin-registration
register_plugin(
    $thisfile,                    // ID of plugin, should be filename minus php
    'Structured Menu',            // Title of plugin
    '1.0',                        // Version of plugin
    'Sven Hofrichter',            // Author of plugin
    'http://www.hofrichter.net/', // Author URL
    'The plugin translates the flat menustructure of the GetSimple-standard-menu'
        . ' into a structured one. The template-designer can fitler for submenus'
        . ' only and can choose between an output as array or "ul"-tagstructure.'
        ,                         // Plugin Description
    'plugin',                     // Page type of plugin
    ''                            // Function that displays content
);

//------------------------------------------------------------------------------
// functions for the templates / function that can be used by the template-developer
/**
 * This function returns the menu structure as an unordered html-list (tag = '<ul>...</ul>').
 * The output can be configured by the use of the funtion-arguments.
 *
 * @param String $slugFilterExpression (optional, default='') is the slug, that we want to display only
 * @param boolean $includePathToRoot (optional, default=true) includes the "path" to the root, if $slugFilterExpression selects a sub-menu
 * @return String the ul-list
 */
function get_structured_menu ($slugFilterExpression = '', $includePathToRoot = true) {
    $result = get_structured_menu_outputHelper($slugFilterExpression, $includePathToRoot);
    return $result['HTML_UL'];
}
/**
 * This function returns the menu structure as an php-array, where the result of the
 * internal getsimple-template-function "menu_data()" will translated into an array
 * with indivdual children (if the individual menu has any). The key for the array of
 * all children-items is 'children'.
 *
 * @param String $slugFilterExpression (optional, default='') is the slug, that we want to display only
 * @param boolean $includePathToRoot (optional, default=true) includes the "path" to the root, if $slugFilterExpression selects a sub-menu
 * @return Array the menu as an structured php-array, with individual children-definitions
 */
function get_structured_menu_array($slugFilterExpression = '', $includePathToRoot = true) {
    $result = get_structured_menu_outputHelper($slugFilterExpression, $includePathToRoot);
    return $result['PHP_ARRAY'];
}


//------------------------------------------------------------------------------
// hookin-functions for the GetSimple-internals 
add_action('changedata-aftersave','structured_menu__flush_cache');
add_action('page-delete', 'structured_menu__flush_cache');

/* deleting and immediately recreating the cache file doesnt quite work that well */
function structured_menu__flush_cache ()
{
  $f = GSDATAOTHERPATH . 'structured_menu_cache.json';
  if (is_file($f)) 
  {
    unlink($f);
  }
}

function structured_menu__reset_cache()  
{
    $f = GSDATAOTHERPATH . 'structured_menu_cache.json';
    if (is_file($f)) {
        unlink($f);
    }    
    // reparse the menu to create a structured one
    // ist nur im Renderkontext verfügbar!!!
    //@require_once($functions);
    if (!function_exists('menu_data')) {
        $functions = GSADMININCPATH . "theme_functions.php";
        @require_once($functions);
    }
    $items = menu_data(); //array("slug"=>$slug,"url"=>$url,"parent_slug"=>$parent,"title"=>$title,"menu_priority"=>$pri,"menu_text"=>$text,"menu_status"=>$menuStatus,"private"=>$private,"pub_date"=>$pubDate);
    // url ............. the full qualified address of the menu-item
    // slug ............ filename
    // parent_slug ..... filename of the parent
    // menu_priority ... the sort order of the menu-item
    // menu_text ....... the title of the menu
    // menu_status ..... 'Y' means enabled and so we want to display it ;-D
    $slugs = array();
    $tmpSlugs = array();
    foreach ($items as $i => $item) {
        if ($item['menu_status'] == 'Y') {
            $tmpSlugs[$item['slug']] = $item;
        }
    }
    $pathes = array();
    foreach ($tmpSlugs as $slug => $item) {
        $path = array($item['slug']);
        while (isset($item['parent_slug']) && strlen($item['parent_slug']) > 0) {
            $item = $tmpSlugs[$item['parent_slug']];
            $path[] = $item['slug'];
        }
        $itemPath = array_reverse($path);
        structured_menu__reset_cache_pathHelper($pathes, $itemPath);
    }
    $mappings['structure'] = $pathes;
    $mappings['objects'] = $tmpSlugs;
    file_put_contents($f, json_encode($mappings, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT|JSON_HEX_AMP));
}

function structured_menu__reset_cache_pathHelper(&$pathes, $itemPath, $counter = 0) {
    if (count($itemPath) <= 0 || $counter >= 50) {
        return;
    }
    $item = array_shift($itemPath);
    if (!isset($pathes[$item])) {
        $pathes[$item] = array();
    }
    structured_menu__reset_cache_pathHelper($pathes[$item], $itemPath, ++$counter);
}

// -----------------------------------------------------------------------------
// helper for the template-functions
function get_structured_menu_outputHelper($slugFilterExpression, $includePathToRoot) {
    $f = GSDATAOTHERPATH . 'structured_menu_cache.json';
    if (!file_exists($f)) {
        structured_menu__reset_cache();
    }
    $mappings  = json_decode(file_get_contents($f), true);
    $structure = $mappings['structure'];
    $objects   = $mappings['objects'];

    if (strlen($slugFilterExpression) > 0) {
        $structure = get_structured_menu_filterPath($structure, $slugFilterExpression, $includePathToRoot);
    }    

    return get_structured_menu_pathToObject($structure, $objects);
}
function get_structured_menu_filterPath($arr, $search, $includeRoot = true) 
{
    if (!is_array($arr) || count($arr) == 0) 
    {
        return array();
    }

    foreach ($arr as $slug => $subArr) 
    {
        if ($slug == $search && count($subArr) > 0) 
        {
            return array($slug => $subArr);
        } 
        elseif (count($subArr) > 0) 
        {
            $foundSubArr = get_structured_menu_filterPath($subArr, $search);
            if (is_array($foundSubArr) && count($foundSubArr) > 0) 
            {
                if ($includeRoot) 
                {
                    return array($slug => $foundSubArr);
                } 
                else 
                {
                    return $foundSubArr;
                }
            }
            elseif (isset ($subArr[$foundSubArr]))
            {
                return array($slug => $arr[$slug]);
            }
        }
        elseif ($slug == $search && count ($subArr) == 0)
        {
            return $slug;
        }
    }
    return array();
}

function _structured_menu__sort_menu_array ($menu)
{
  $menu_priority = array ();
  foreach ($menu as $key => $row)
  {
    $menu_priority[$key] = $row['menu_priority'];
  }
  array_multisort($menu_priority, SORT_ASC, $menu);
  return $menu;
}

function get_structured_menu_pathToObject($structure, $objects, $depth = 0) {
    $result['PHP_ARRAY'] = array();
    $result['HTML_UL'] = str_repeat('  ', $depth) . '<ul class="menu-level-' . $depth . '">';
    $depth += 1;
    $itemId = 0;
    foreach ($structure as $key => $arr) {
        $result['PHP_ARRAY'][$key] = $objects[$key];
        $result['HTML_UL'] .= '<li class="menu-item-' . ($itemId++) . '"><a href="?id=' . $objects[$key]['slug'] . '" title="' . $objects[$key]['menu_text'] . '">' . $objects[$key]['menu_text'] . '</a>';
        if (is_array($arr) && count($arr) > 0) {
            $arr = get_structured_menu_pathToObject($structure[$key], $objects, $depth);
            $result['PHP_ARRAY'][$key]['children'] = $arr['PHP_ARRAY'];
            $result['HTML_UL'] .= "\n" . $arr['HTML_UL'];
        }
        $result['HTML_UL'] .= '</li>';
    }
    $result['HTML_UL'] .= "</ul>\n";
    $result['PHP_ARRAY'] = _structured_menu__sort_menu_array ($result['PHP_ARRAY']);
    return $result;
}
?>
