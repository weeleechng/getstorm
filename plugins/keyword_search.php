<?php

/*
Plugin Name: GetSimple Keyword Search
Description: Search content for keywords
Version: 1.0.1
Author: Rogier Koppejan
*/

# get correct id for plugin
$thisfile = basename(__FILE__, '.php');

# register plugin
register_plugin(
  $thisfile,
  'Keyword Search',
  '1.0.1',
  'Rogier Koppejan',
  '#',
  'Search content for keywords',
  'plugins'
);


# definitions
define('EXCERPTLENGTH', 30);


/*******************************************************
 * @function get_search_results()
 * @action filter site contents for given keywords
 */
function get_search_results() {
  $keywords = @explode(' ', $_POST['keywords']);
  $pages = get_pages();

  // Find matching documents
  foreach ($keywords as $keyword) {
    $match = array();
    foreach ($pages as $page) {
      $data = getXML($page);
      if ($data->private != 'Y') {
        if (stripos($data->content, $keyword) !== false)
          $match[] = $page;
      }
    }
    $pages = $match;
  }

  // Print results
  if (count($pages) > 0) 
  {
    echo '<p>Search results for <code>' . implode (' ', $keywords) . '</code>:</p>';
    print_search_results ($pages);
  } 
  else 
  {
    if ($keywords != '')
    {
      echo '<p>Your search for <code>' . implode (' ', $keywords) . '</code> returned no results.</p>';  
    }
    else
    {
      echo '<p>Please enter a search term.</p>';  
    }
  }
}


/*******************************************************
 * @function print_search_results()
 * @action pretty prints the search results
 */
function print_search_results($pages) {
  global $PRETTYURLS;
  global $SITEURL;

  echo '<ul id="search_results">';
  foreach ($pages as $page) {
    $data = getXML($page);
    $url = ($PRETTYURLS == 1) ? $SITEURL . $data->url : $SITEURL . 'index.php?id=' . $data->url;
    // Extract and filter content
    $content = preg_replace('/&#?[a-z0-9]{2,8};/i', '', stripslashes(strip_tags(html_entity_decode($data->content, ENT_QUOTES, 'UTF-8'))));
    $content = preg_replace('/\s{2,}/', ' ', trim($content));
    // Create an excerpt of the content
    $tokens = explode(' ', $content, EXCERPTLENGTH);
    array_pop($tokens);
    $content = implode(' ', $tokens);
    // Print result
    echo '<li><a href="' . $url . '" /><b>' . $data->title . '</b></a><br />' . $content . ' [...]</li>';
  }
  echo '</ul>';
}


/*******************************************************
 * @function get_pages()
 * @returns an array with current pages
 */
function get_pages() {
  $path = GSDATAPAGESPATH;
  $files = getFiles($path);
  $pages = array();
  foreach ($files as $file) {
    if (isFile($file, $path, 'xml')) {
      $pages[] = $path . $file;
    }
  }
  return $pages;
}


?>
